import java.sql.*;
import java.util.ArrayList;

public class StudentEntityManager {

    private static String database_url = "jdbc:mysql://localhost:3306/school?useSSL=false&allowPublicKeyRetrieval=true";
    private static String database_user = "root";
    private static String database_pass = "123qwe";
    private static Connection db;

    public StudentEntityManager() {
        try {
            db = DriverManager.getConnection(database_url, database_user, database_pass);
            if(!db.isClosed()) System.out.println("You are connected to the DB " + db.getCatalog());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void save(Student entity) {
        String fname = entity.getFirstname();
        String lname = entity.getLastname();
        Integer year = entity.getYear_of_birth();
        Float mark = entity.getAverage_mark();

        String sql = "INSERT INTO students(firstname, lastname, year, avermark) " +
                "VALUES('" + fname + "','" + lname + "'," + year + "," + mark + ")";
        Statement st;
        try {
            st = db.createStatement();
            st.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void delete(Student entity) {
        String sql = "DELETE FROM students WHERE lastname='" + entity.getLastname() +"'";
        //String sql = "DELETE FROM students WHERE id=" + entity.getId();
        Statement st;
        try {
            st = db.createStatement();
            st.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Student> all(){
        ArrayList<Student> list = new ArrayList<Student>();
        String sql = "SELECT * FROM students";
        Statement st;
        try {
            st = db.createStatement();
            ResultSet r = st.executeQuery(sql);
            while (r.next()) {
                /*System.out.println(r.getInt("id") + " | "
                        + r.getString("firstname") + " | "
                        + r.getString("lastname")) + " | "
                        + r.getInt("year") + " | "
                        + r.getFloat("avermark");
                */
                Student student = new Student(r.getString("firstname"),
                        r.getString("lastname"),
                        r.getInt("year"), r.getFloat("avermark"));
                list.add((student));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
