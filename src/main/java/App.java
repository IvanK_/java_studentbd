public class App {
    public static void main(String[] args) {

        StudentEntityManager sem = new StudentEntityManager();

        Student student1 = new Student("Igor", "Petrov", 1990, 22f);
        Student student2 = new Student("Vasea", "Ivanov", 1980, 122f);

        StudentEntityManager.save(student1);
        StudentEntityManager.save(student2);

        System.out.println("ArrayList before delete:");
        System.out.println(StudentEntityManager.all());

        System.out.println("ArrayList after delete:");
        StudentEntityManager.delete(student1);
        System.out.println(StudentEntityManager.all());
    }
}
